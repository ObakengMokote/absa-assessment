package com.psybergate.absa.scenario1.service;

import com.psybergate.absa.scenario1.entity.ClientTransaction;
import com.psybergate.absa.scenario1.entity.Transaction;
import com.psybergate.absa.scenario1.enumerator.MessageStatus;
import com.psybergate.absa.scenario1.enumerator.SubService;
import com.psybergate.absa.scenario1.repository.TransactionRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TransactionServiceImplTest {

    @Mock
    private TransactionService service;

    @Mock
    private TransactionRepository repository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldGetAllTransactions_When_ThereAreTransactionsInDatabase() {
        Transaction t1 = mock(Transaction.class);
        Transaction t2 = mock(Transaction.class);
        Transaction t3 = mock(Transaction.class);
        Transaction t4 = mock(Transaction.class);
        Transaction t5 = mock(Transaction.class);
        List<Transaction> expected = Arrays.asList(t1, t2, t3, t4, t5);

        when(service.getTransactions()).thenReturn(Arrays.asList(t1, t2, t3, t4, t5));

        List<Transaction> result = service.getTransactions();

        assertEquals(expected, result);
    }

    @Test
    public void shouldGetAllTransactionsFromPreviousMonth_When_ThereAreAnySuchTransactionsAvailable() {
        Transaction t1 = mock(Transaction.class);
        Transaction t2 = mock(Transaction.class);
        Transaction t3 = mock(Transaction.class);
        Transaction t4 = mock(Transaction.class);
        Transaction t5 = mock(Transaction.class);
        List<Transaction> expected = Arrays.asList(t2, t4);

        when(t1.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2014, 8, 25, 14, 15, 0)));
        when(t2.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2014, 11, 25, 16, 0, 0)));
        when(t3.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2019, 12, 1, 12, 0, 0)));
        when(t4.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2019, 11, 15, 6, 0, 0)));
        when(t5.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2018, 4, 25, 17, 30, 18)));
        when(service.getPreviousMonthsTransactions()).thenReturn(expected);

        List<Transaction> result = service.getPreviousMonthsTransactions();

        assertEquals(expected, result);
    }

    @Test
    public void shouldGetAllTransactionsFromPreviousMonthThatAreCompleted_When_ThereAreAnySuchTransactionsAvailable() {
        Transaction t1 = mock(Transaction.class);
        Transaction t2 = mock(Transaction.class);
        Transaction t3 = mock(Transaction.class);
        Transaction t4 = mock(Transaction.class);
        Transaction t5 = mock(Transaction.class);
        MessageStatus messageStatus = MessageStatus.COMPLETED;
        List<Transaction> expected = Arrays.asList(t2);

        when(t1.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2019, 8, 25, 0, 0, 0)));
        when(t2.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2019, 11, 25, 14, 18, 12)));
        when(t3.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2019, 11, 1, 0, 0, 0)));
        when(t4.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2019, 11, 15, 10, 0, 22)));
        when(t5.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2019, 4, 25, 23, 12, 16)));
        when(t1.getMessageStatus()).thenReturn(MessageStatus.COMPLETED);
        when(t2.getMessageStatus()).thenReturn(MessageStatus.COMPLETED);
        when(t3.getMessageStatus()).thenReturn(MessageStatus.PENDING);
        when(t4.getMessageStatus()).thenReturn(MessageStatus.RECEIVED);
        when(t5.getMessageStatus()).thenReturn(MessageStatus.RECEIVED);
        when(service.getPreviousMonthsTransactionsByStatus(MessageStatus.COMPLETED)).thenReturn(expected);

        List<Transaction> result = service.getPreviousMonthsTransactionsByStatus(messageStatus);

        assertEquals(expected, result);
    }

    @Test
    public void shouldGetAllTransactionsFromPreviousMonthThatAreReceived_When_ThereAreAnySuchTransactionsAvailable() {
        Transaction t1 = mock(Transaction.class);
        Transaction t2 = mock(Transaction.class);
        Transaction t3 = mock(Transaction.class);
        Transaction t4 = mock(Transaction.class);
        Transaction t5 = mock(Transaction.class);
        MessageStatus messageStatus = MessageStatus.COMPLETED;
        List<Transaction> expected = Arrays.asList(t4);

        when(t1.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2019, 8, 25, 0, 0, 0)));
        when(t2.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2019, 11, 25, 17, 30, 26)));
        when(t3.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2019, 11, 1, 16, 0, 0)));
        when(t4.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2019, 11, 15, 12, 0, 0)));
        when(t5.getDateTimeCreated()).thenReturn(Timestamp.valueOf(LocalDateTime.of(2019, 4, 25, 21, 16, 32)));
        when(t1.getMessageStatus()).thenReturn(MessageStatus.COMPLETED);
        when(t2.getMessageStatus()).thenReturn(MessageStatus.COMPLETED);
        when(t3.getMessageStatus()).thenReturn(MessageStatus.PENDING);
        when(t4.getMessageStatus()).thenReturn(MessageStatus.RECEIVED);
        when(t5.getMessageStatus()).thenReturn(MessageStatus.RECEIVED);
        when(service.getPreviousMonthsTransactionsByStatus(MessageStatus.COMPLETED)).thenReturn(expected);

        List<Transaction> result = service.getPreviousMonthsTransactionsByStatus(messageStatus);

        assertEquals(expected, result);
    }

    @Test
    public void shouldReturnMapWithZAROUT1AndCCYOUT3ForSwiftAddressFIRNZAJJ_When_FIRNZAJJHas1CompleteDomesticTransactionAnd3CompleteInternationalTransactions() {
        Transaction t1 = mock(Transaction.class);
        Transaction t2 = mock(Transaction.class);
        Transaction t3 = mock(Transaction.class);
        Transaction t4 = mock(Transaction.class);
        Transaction t5 = mock(Transaction.class);
        Transaction t6 = mock(Transaction.class);

        String key1 = "FIRNZAJJZAROUT";
        String key2 = "FIRNZAJJCCYOUT";

        ClientTransaction ct1 = new ClientTransaction("FIRNZAJJ", SubService.DOMESTIC);
        ClientTransaction ct2 = new ClientTransaction("FIRNZAJJ", SubService.INTERNATIONAL);
        ct2.incrementNumTransactions();
        ct2.incrementNumTransactions();

        List<Transaction> receivedTransactions = Arrays.asList(t1, t3);
        List<Transaction> completedTransactions = Arrays.asList(t4, t5, t6);
        Map<String, ClientTransaction> expected = new HashMap<>();
        expected.put(key1, ct1);
        expected.put(key2, ct2);

        when(t1.getClientSwiftAddress()).thenReturn("FIRNZAJJ");
        when(t2.getClientSwiftAddress()).thenReturn("FIRNZAJJ");
        when(t3.getClientSwiftAddress()).thenReturn("CABLZAJJ");
        when(t4.getClientSwiftAddress()).thenReturn("FIRNZAJJ");
        when(t5.getClientSwiftAddress()).thenReturn("FIRNZAJJ");
        when(t6.getClientSwiftAddress()).thenReturn("FIRNZAJJ");

        when(t1.getMessageStatus()).thenReturn(MessageStatus.RECEIVED);
        when(t2.getMessageStatus()).thenReturn(MessageStatus.PENDING);
        when(t3.getMessageStatus()).thenReturn(MessageStatus.RECEIVED);
        when(t4.getMessageStatus()).thenReturn(MessageStatus.COMPLETED);
        when(t5.getMessageStatus()).thenReturn(MessageStatus.COMPLETED);
        when(t6.getMessageStatus()).thenReturn(MessageStatus.COMPLETED);

        when(t1.getCurrency()).thenReturn("ZAR");
        when(t2.getCurrency()).thenReturn("ZAR");
        when(t3.getCurrency()).thenReturn("ZAR");
        when(t4.getCurrency()).thenReturn("USD");
        when(t5.getCurrency()).thenReturn("GBP");
        when(t6.getCurrency()).thenReturn("GBP");

        when(service.getPreviousMonthsTransactionsByStatus(MessageStatus.RECEIVED)).thenReturn(receivedTransactions);
        when(service.getPreviousMonthsTransactionsByStatus(MessageStatus.COMPLETED)).thenReturn(completedTransactions);
        when(service.previousMonthsCompletedTransactions()).thenReturn(expected);

        Map<String, ClientTransaction> clientTransactionMap = service.previousMonthsCompletedTransactions();

        assertEquals(2, clientTransactionMap.size());
        assertEquals("INTEGRATEDSERVICES FIRNZAJJ " + SubService.DOMESTIC + " " + ct1.getDateOfCreation() + " " + 1, clientTransactionMap.get(key1).toString());
        assertEquals("INTEGRATEDSERVICES FIRNZAJJ " + SubService.INTERNATIONAL + " " + ct2.getDateOfCreation() + " " + 3, clientTransactionMap.get(key2).toString());
    }
}