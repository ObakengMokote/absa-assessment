package com.psybergate.absa.scenario1.scheduler;

import org.junit.Test;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.support.CronTrigger;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class TransactionReadingSchedulerCronExpressionTest {

    @Test
    public void shouldGive__2019_01_01__AsNextExecutionTime_When_LastExecutionTimeWas__2019_11_01() {
        String expected = "Scheduler will next run on 2019-12-02 07:00:00";
        CronTrigger trigger = new CronTrigger("0 0 7 1 * MON-FRI");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date lastExecutionTime = (Date) (java.sql.Date.valueOf(LocalDate.of(2019, 11, 3)));

        Date nextExecutionTime = trigger.nextExecutionTime(new TriggerContext() {
            @Override
            public Date lastScheduledExecutionTime() {
                return lastExecutionTime;
            }

            @Override
            public Date lastActualExecutionTime() {
                return lastExecutionTime;
            }

            @Override
            public Date lastCompletionTime() {
                return lastExecutionTime;
            }
        });

        String actual = "Scheduler will next run on " + dateFormat.format(nextExecutionTime);

        assertEquals(expected, actual);
    }

    @Test
    public void shouldGive__2020_01_01__AsNextExecutionTime_When_LastExecutionTimeWas__2019_12_01() {
        String expected = "Scheduler will next run on 2020-01-01 07:00:00";
        CronTrigger trigger = new CronTrigger("0 0 7 1 * MON-FRI");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date lastExecutionTime = (Date) (java.sql.Date.valueOf(LocalDate.of(2019, 12, 2)));

        Date nextExecutionTime = trigger.nextExecutionTime(new TriggerContext() {
            @Override
            public Date lastScheduledExecutionTime() {
                return lastExecutionTime;
            }

            @Override
            public Date lastActualExecutionTime() {
                return lastExecutionTime;
            }

            @Override
            public Date lastCompletionTime() {
                return lastExecutionTime;
            }
        });

        String actual = "Scheduler will next run on " + dateFormat.format(nextExecutionTime);

        assertEquals(expected, actual);
    }
}