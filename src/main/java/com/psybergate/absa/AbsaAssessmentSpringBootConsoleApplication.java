package com.psybergate.absa;

import com.psybergate.absa.scenario1.controller.TransactionController;
import com.psybergate.absa.scenario2.Scenario2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;
import java.util.Scanner;

@EnableScheduling
@ComponentScan({"com.psybergate.absa", "com.psybergate.fileupload"})
@EntityScan({"com.psybergate.absa", "com.psybergate.fileupload"})
@EnableJpaRepositories({"com.psybergate.absa", "com.psybergate.fileupload"})
@SpringBootApplication
public class AbsaAssessmentSpringBootConsoleApplication implements CommandLineRunner {
    private static TransactionController transactionController;

    @Autowired
    public AbsaAssessmentSpringBootConsoleApplication(TransactionController transactionController) {
        this.transactionController = transactionController;
    }

    public static void main(String[] args) {
        SpringApplication.run(AbsaAssessmentSpringBootConsoleApplication.class, args);
    }

    private static void scenario1() throws IOException {
        transactionController.createUsageDataFile();
    }

    private static void scenario2() {
        Scenario2.filterStatusesWithSequentialStream();
        System.out.println("\n");
        Scenario2.filterStatusesWithParallelStream();
    }

    @Override
    public void run(String... args) throws Exception {
        Scanner input = null;
        String choice = "";

        do {
            System.out.println("Select Scenario to run \n(1) Scenario 1 \n(2) Scenario 2 \n(x) Exit \n");
            input = new Scanner(System.in);
            choice = input.next();

            switch (choice) {
                case "1": {
                    scenario1();
                    System.out.println("\n\n");
                    break;
                }
                case "2": {
                    scenario2();
                    System.out.println("\n");
                    break;
                }
                case "x": {
                    System.exit(-1);
                }
                default: {
                    System.out.println("Invalid option selected. Try again.");
                    break;
                }
            }
        } while (!choice.equalsIgnoreCase("x"));
    }
}