package com.psybergate.absa.scenario1.repository;

import com.psybergate.absa.scenario1.entity.ClientTransaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientTransactionRepository extends CrudRepository<ClientTransaction, Long> {
}
