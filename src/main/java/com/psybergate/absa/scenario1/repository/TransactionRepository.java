package com.psybergate.absa.scenario1.repository;

import com.psybergate.absa.scenario1.entity.Transaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    @Query("select t from Transaction t where date_time_created BETWEEN :startMonthDate AND :endMonthDate")
    List<Transaction> findByMonth(@Param("startMonthDate") Timestamp startMonthDate, @Param("endMonthDate") Timestamp endMonthDate);

}
