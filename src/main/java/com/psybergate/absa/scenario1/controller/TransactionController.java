package com.psybergate.absa.scenario1.controller;

import com.psybergate.absa.scenario1.entity.Transaction;
import com.psybergate.absa.scenario1.enumerator.MessageStatus;
import com.psybergate.absa.scenario1.scheduler.TransactionReadingScheduler;
import com.psybergate.absa.scenario1.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;

@RestController
@RequestMapping("/transaction")
public class TransactionController {
    private final TransactionService service;
    private final TransactionReadingScheduler transactionReadingScheduler;

    @Autowired
    public TransactionController(TransactionService service, TransactionReadingScheduler transactionReadingScheduler) {
        this.service = service;
        this.transactionReadingScheduler = transactionReadingScheduler;
        initialiseDB();
    }

    private void initialiseDB() {
        Arrays.asList(
                new Transaction("049969", "ABSAZAJJ", MessageStatus.RECEIVED, "ZAR", 21000, Timestamp.valueOf("2019-11-01 16:00:34")),
                new Transaction("049969", "ABSAZAJJ", MessageStatus.PENDING, "ZAR", 6200, Timestamp.valueOf("2019-11-01 23:30:15")),
                new Transaction("049969", "ABSAZAJJ", MessageStatus.RECEIVED, "ZAR", 21000, Timestamp.valueOf("2019-12-25 16:00:34")),
                new Transaction("920584", "SBZAZAJJ", MessageStatus.COMPLETED, "AOA", 11000, Timestamp.valueOf("2019-01-16 16:00:00")),
                new Transaction("920584", "SBZAZAJJ", MessageStatus.COMPLETED, "AOA", 11000, Timestamp.valueOf("2019-11-16 16:00:00")),
                new Transaction("195432", "SBZAZAJJ", MessageStatus.RECEIVED, "ZAR", 8000, Timestamp.valueOf("2019-11-25 16:00:00")),
                new Transaction("591126", "ABSAZAJJ", MessageStatus.PENDING, "GBP", 10000, Timestamp.valueOf("2009-02-25 00:00:00")),
                new Transaction("602785", "FIRNZAJJ", MessageStatus.PENDING, "ZAR", 500, Timestamp.valueOf("2018-09-15 01:00:00")),
                new Transaction("602785", "FIRNZAJJ", MessageStatus.PENDING, "USD", 150_000, Timestamp.valueOf("2019-11-30 14:12:32")),
                new Transaction("6921481", "ABSAZAJJ", MessageStatus.RECEIVED, "ZAR", 425_000, Timestamp.valueOf("2017-12-04 16:00:00")),
                new Transaction("6921481", "SBZAZAJJ", MessageStatus.RECEIVED, "USN", 14000, Timestamp.valueOf("2011-01-06 10:00:00")),
                new Transaction("349826", "ABSAZAJJ", MessageStatus.RECEIVED, "ZAR", 11000, Timestamp.valueOf("2019-11-25 21:22:14")),
                new Transaction("126928", "ABSAZAJJ", MessageStatus.PENDING, "ZAR", 20000, Timestamp.valueOf("2019-11-01 13:30:15")),
                new Transaction("315547", "CABLZAJJ", MessageStatus.RECEIVED, "ZAR", 500, Timestamp.valueOf("2019-12-25 16:00:00")),
                new Transaction("920584", "SBZAZAJJ", MessageStatus.COMPLETED, "AOA", 11000, Timestamp.valueOf("2019-01-16 16:00:00")),
                new Transaction("920584", "SBZAZAJJ", MessageStatus.COMPLETED, "AOA", 11000, Timestamp.valueOf("2019-11-16 16:00:00")),
                new Transaction("875472", "BHAUSJJ", MessageStatus.COMPLETED, "USD", 8000, Timestamp.valueOf("2019-11-25 16:00:00")),
                new Transaction("591126", "ABSAZAJJ", MessageStatus.PENDING, "GBP", 16000, Timestamp.valueOf("2009-02-25 00:00:00")),
                new Transaction("602785", "FIRNZAJJ", MessageStatus.PENDING, "ZAR", 4500, Timestamp.valueOf("2018-09-15 01:00:00")),
                new Transaction("611304", "NEDSZAJj", MessageStatus.COMPLETED, "USD", 100_000, Timestamp.valueOf("2019-11-30 14:16:12")),
                new Transaction("1361521", "ABSAZAJJ", MessageStatus.RECEIVED, "ZAR", 425_000, Timestamp.valueOf("2017-12-04 16:00:00")),
                new Transaction("7121540", "SBZAZAJJ", MessageStatus.RECEIVED, "USN", 14_000, Timestamp.valueOf("2011-01-06 10:00:00"))
        ).forEach(t -> service.saveTransaction(t));
    }

    public void createUsageDataFile() throws IOException {
        transactionReadingScheduler.getLastMonthsUsageRecords();
    }
}
