package com.psybergate.absa.scenario1.enumerator;

public enum MessageStatus {
    PENDING("Pending"),
    RECEIVED("Received"),
    AWAITING_MATURITY("Awaiting Maturity"),
    COMPLETED("Completed");

    private String name;

    private MessageStatus(String name){
        this.name = name;
    }
}
