package com.psybergate.absa.scenario1.enumerator;

public enum SubService {
    DOMESTIC("ZAROUT"), INTERNATIONAL("CCYOUT");

    private String outputName;

    private SubService(String outputName) {
        this.outputName = outputName;
    }

    @Override
    public String toString() {
        return outputName;
    }
}
