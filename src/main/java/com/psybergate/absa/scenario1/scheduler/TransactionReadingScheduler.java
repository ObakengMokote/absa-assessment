package com.psybergate.absa.scenario1.scheduler;

import com.psybergate.absa.scenario1.entity.ClientTransaction;
import com.psybergate.absa.scenario1.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;
import java.util.Set;

@Component
@Transactional
public class TransactionReadingScheduler {
    private final TransactionService transactionService;

    @Autowired
    public TransactionReadingScheduler(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    private void uploadFile(File file) {
        System.out.println("Uploading file " + file.getName() + " to 'SharePoint'...");
        String uri = "http://localhost:8686/file/upload";
        RestTemplate restTemplate = new RestTemplate();
        File result = restTemplate.postForObject(uri, file, File.class);
        System.out.println("File uploaded to 'SharePoint'");
    }

    @Scheduled(cron = "0 0 7 1 * MON-FRI", zone = "Africa/Johannesburg")
    public void getLastMonthsUsageRecords() throws IOException {
        StringBuilder sb = new StringBuilder();
        String fileSeparator = System.getProperty("file.separator");
        String fileDirectory = System.getProperty("user.dir") + fileSeparator + "UsageRecords" + fileSeparator + "usageRecord_" + LocalDate.now().toString() + ".txt";
        File file = new File(fileDirectory);
        Map<String, ClientTransaction> clientTransactionMap = transactionService.previousMonthsCompletedTransactions();
        Set<Map.Entry<String, ClientTransaction>> entries = (clientTransactionMap != null) ? clientTransactionMap.entrySet() : null;

        System.out.println("Creating file from database entries....");

        if ((entries != null) && (!entries.isEmpty())) {
            for (Map.Entry entry : entries) {
                sb.append(entry.getValue().toString()).append("\n");
            }
        } else {
            sb.append("No transactions made last month.");
        }

        FileWriter fileWriter = null;

        try {
            fileWriter = new FileWriter(file);
            fileWriter.write(sb.toString());
            fileWriter.close();
        } catch (IOException ioex) {
            throw ioex;
        }

        System.out.println("File successfully created!");
        uploadFile(file);
    }
}