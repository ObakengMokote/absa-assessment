package com.psybergate.absa.scenario1.service;

import com.psybergate.absa.scenario1.entity.ClientTransaction;
import com.psybergate.absa.scenario1.entity.Transaction;
import com.psybergate.absa.scenario1.enumerator.MessageStatus;
import com.psybergate.absa.scenario1.enumerator.SubService;
import com.psybergate.absa.scenario1.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepository transactionRepository;
    private final ClientTransactionService clientTransactionService;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, ClientTransactionService clientTransactionService) {
        this.transactionRepository = transactionRepository;
        this.clientTransactionService = clientTransactionService;
    }

    @Override
    public Transaction saveTransaction(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    @Override
    public List<Transaction> getTransactions() {
        return (List<Transaction>) transactionRepository.findAll();
    }

    @Override
    public List<Transaction> getMonthTransactions(int year, int monthNumber) {
        Timestamp monthStart = Timestamp.valueOf(LocalDateTime.of(year, monthNumber, 1, 0, 0, 0));
        int lastDayOfMonth = monthStart.toLocalDateTime().toLocalDate().lengthOfMonth();
        Timestamp monthEnd = Timestamp.valueOf(LocalDateTime.of(year, monthNumber, lastDayOfMonth, 23, 59, 59));
        System.out.println("First day of November: " + monthStart.toString());
        System.out.println("Last day of November: " + monthEnd.toString());
        return transactionRepository.findByMonth(monthStart, monthEnd);
    }

    @Override
    public List<Transaction> getMonthTransactionsByStatus(int year, int monthNumber, MessageStatus status) {
        return getMonthTransactions(year, monthNumber).stream().filter(t -> t.getMessageStatus().equals(status)).collect(Collectors.toList());
    }

    @Override
    public List<Transaction> getPreviousMonthsTransactions() {
        int year = LocalDate.now().getYear();
        int month = LocalDate.now().getMonthValue() - 1;
        return getMonthTransactions(year, month);
    }

    @Override
    public List<Transaction> getPreviousMonthsTransactionsByStatus(MessageStatus status) {
        int year = LocalDate.now().getYear();
        int month = LocalDate.now().getMonthValue() - 1;
        return getMonthTransactionsByStatus(year, month, status);
    }

    private SubService determineSubService(Transaction transaction) {
        return (transaction.getCurrency().equals("ZAR")) ? SubService.DOMESTIC : SubService.INTERNATIONAL;
    }

    @Override
    public Map<String, ClientTransaction> previousMonthsCompletedTransactions() {
        Map<String, ClientTransaction> clientTransactionMap = new HashMap<>();
        List<Transaction> completedTransactions = getPreviousMonthsTransactionsByStatus(MessageStatus.RECEIVED);
        completedTransactions.addAll(getPreviousMonthsTransactionsByStatus(MessageStatus.COMPLETED));

        completedTransactions.forEach(t -> System.out.println(t.toString()));

        for (Transaction transaction : completedTransactions) {
            String key = transaction.getClientSwiftAddress() + determineSubService(transaction).toString();
            ClientTransaction clientTransaction = null;

            if (clientTransactionMap.containsKey(key)) {
                clientTransaction = clientTransactionMap.get(key);
                clientTransaction.incrementNumTransactions();
                clientTransactionMap.replace(key, clientTransaction);
            } else {
                clientTransaction = new ClientTransaction(transaction.getClientSwiftAddress(), determineSubService(transaction));
                clientTransactionMap.put(key, clientTransaction);
            }

            if (clientTransaction != null) {
                clientTransactionService.saveClientTransaction(clientTransaction);
            }
        }

        return clientTransactionMap;
    }
}