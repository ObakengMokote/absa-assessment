package com.psybergate.absa.scenario1.service;

import com.psybergate.absa.scenario1.entity.ClientTransaction;
import com.psybergate.absa.scenario1.entity.Transaction;
import com.psybergate.absa.scenario1.enumerator.MessageStatus;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface TransactionService {

    Transaction saveTransaction(Transaction transaction);

    List<Transaction> getTransactions();

    List<Transaction> getMonthTransactions(int year, int monthNumber);

    List<Transaction> getMonthTransactionsByStatus(int year, int monthNumber, MessageStatus status);

    List<Transaction> getPreviousMonthsTransactions();

    List<Transaction> getPreviousMonthsTransactionsByStatus(MessageStatus status);

    Map<String, ClientTransaction> previousMonthsCompletedTransactions();

}
