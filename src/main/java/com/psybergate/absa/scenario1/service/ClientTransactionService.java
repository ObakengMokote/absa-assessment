package com.psybergate.absa.scenario1.service;

import com.psybergate.absa.scenario1.entity.ClientTransaction;

public interface ClientTransactionService {
    ClientTransaction saveClientTransaction(ClientTransaction clientTransaction);
}
