package com.psybergate.absa.scenario1.service;

import com.psybergate.absa.scenario1.entity.ClientTransaction;
import com.psybergate.absa.scenario1.repository.ClientTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class ClientTransactionServiceImpl implements ClientTransactionService {
    private final ClientTransactionRepository repository;

    @Autowired
    public ClientTransactionServiceImpl(ClientTransactionRepository repository) {
        this.repository = repository;
    }

    @Override
    public ClientTransaction saveClientTransaction(ClientTransaction clientTransaction){
        return repository.save(clientTransaction);
    }
}
