package com.psybergate.absa.scenario1.entity;

import com.psybergate.absa.scenario1.enumerator.MessageStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "transaction")
public class Transaction {

    @Getter
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @Column(name = "transaction_reference", columnDefinition = "VARCHAR(16)", nullable = false)
    private String transactionReference;

    @Getter
    @Setter
    @Column(name = "client_swift_address", columnDefinition = "VARCHAR(12)", nullable = false)
    private String clientSwiftAddress;

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    @Column(name = "message_status", nullable = false)
    private MessageStatus messageStatus;

    @Getter
    @Setter
    @Column(name = "currency", columnDefinition = "VARCHAR(3)", nullable = false)
    private String currency;

    @Getter
    @Setter
    @Column(name = "amount", nullable = false)
    private Integer amount;

    @Getter
    @Setter
    @Column(name = "date_time_created", nullable = false)
    private Timestamp dateTimeCreated;

    public Transaction() { }

    public Transaction(String transactionReference, String clientSwiftAddress, MessageStatus messageStatus, String currency, Integer amount, Timestamp dateTimeCreated) {
        this.transactionReference = transactionReference;
        this.clientSwiftAddress = clientSwiftAddress;
        this.messageStatus = messageStatus;
        this.currency = currency;
        this.amount = amount;
        this.dateTimeCreated = dateTimeCreated;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !obj.getClass().equals(getClass())) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        Transaction otherTransaction = (Transaction) obj;

        if ((id != null) && (otherTransaction.id == null) || !id.equals(otherTransaction.id)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", transactionReference='" + transactionReference + '\'' +
                ", clientSwiftAddress='" + clientSwiftAddress + '\'' +
                ", messageStatus=" + messageStatus +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                ", dateTimeCreated=" + dateTimeCreated +
                '}';
    }
}