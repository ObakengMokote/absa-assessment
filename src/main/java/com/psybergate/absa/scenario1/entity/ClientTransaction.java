package com.psybergate.absa.scenario1.entity;

import com.psybergate.absa.scenario1.enumerator.SubService;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

@Entity
@Table(name = "client_transaction")
public class ClientTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(name = "service_name")
    private static final String SERVICE_NAME = "INTEGRATEDSERVICES";

    @Getter
    @Column(name = "swift_address")
    private String swiftAddress;

    @Getter
    @Enumerated(EnumType.STRING)
    @Column(name = "sub_servive")
    private SubService subService;

    @Getter
    @Column(name = "date_of_creation")
    private final Timestamp dateOfCreation;

    @Getter
    @Column(name = "num_completed_transactions")
    private int numTransactions = 1;

    public ClientTransaction(String swiftAddress, SubService subService) {
        this.swiftAddress = swiftAddress;
        this.subService = subService;
        dateOfCreation = Timestamp.valueOf(LocalDateTime.now());
    }

    public void incrementNumTransactions() {
        numTransactions++;
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return SERVICE_NAME + " " + swiftAddress + " " + subService + " " + dateFormat.format(dateOfCreation) + " " + numTransactions;
    }
}
