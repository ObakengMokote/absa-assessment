package com.psybergate.absa.scenario2;

import java.util.Arrays;
import java.util.List;

public class Scenario2 {
    private static final List<String> STATUSES = Arrays.asList("Received", "Pending", "", "Awaiting Maturity", "Completed", "");

    public static void filterStatusesWithSequentialStream() {
        Long startTime = System.currentTimeMillis();
        STATUSES.stream().filter(s -> !s.isEmpty()).forEach(status -> System.out.println(status));
        System.out.println("\nTime taken to filter sequential stream: " + (System.currentTimeMillis() - startTime) + " milliseconds");
    }

    public static void filterStatusesWithParallelStream() {
        Long startTime = System.currentTimeMillis();
        STATUSES.parallelStream().filter(s -> !s.isEmpty()).forEach(status -> System.out.println(status));
        System.out.println("\nTime taken to filter parallel stream: " + (System.currentTimeMillis() - startTime) + " milliseconds");
    }
}
