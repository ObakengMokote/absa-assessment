package com.psybergate.fileupload.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/file")
public class FileStorageController {
    @PostMapping("/upload")
    public ResponseEntity<String> uploadFile(@RequestBody File file) throws IOException {
        String fileSeparator = System.getProperty("file.separator");
        Path filePath = Files.createFile(Paths.get(
                System.getProperty("user.dir") + fileSeparator + "UsageRecords" + fileSeparator + "TransactionRecords" + fileSeparator + file.getName()));
        Files.write(filePath, Files.readAllBytes(Paths.get(file.getAbsolutePath())));
        return new ResponseEntity(HttpStatus.CREATED);
    }
}